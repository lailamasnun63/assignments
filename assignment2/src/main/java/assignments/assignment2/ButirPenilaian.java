package assignments.assignment2;

public class ButirPenilaian{
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(Double nilai, boolean terlambat){
        this.terlambat = terlambat;
        this.nilai = Math.max(0, nilai);
    }
    public ButirPenilaian(int nilai, boolean terlambat){
        this.terlambat = terlambat;
        this.nilai = Math.max(0, nilai);
    }

    public double getNilai(){
        if (this.terlambat == true){
            return this.penguranganNilai();
        }else{
            return this.nilai;
        }
    }

    public double penguranganNilai(){
        double besarPenalty = this.nilai * 0.2;
        double nilaiAkhir = this.nilai - besarPenalty;
        return nilaiAkhir;
    }

    public String toString(){
        if(this.terlambat == true){
            return String.format("%.2f (T)", this.getNilai());
        }else{
            return String.format("%.2f", this.getNilai());
        }
    }
}
