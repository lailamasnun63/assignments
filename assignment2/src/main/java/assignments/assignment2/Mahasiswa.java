package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa>{
    private KomponenPenilaian[] komponenPenilaian;
    private String npm;
    private String nama;


    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian){
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen){
        for(int i = 0; i < this.komponenPenilaian.length; i++){
            if(this.komponenPenilaian[i].getNama().equals(namaKomponen)){
                return komponenPenilaian[i];
            }
        }
        return null;
    }

    public int compareTo(Mahasiswa other){
        return this.npm.compareTo(other.npm);
    }

    public String rekap(){
        String temp = "";
        double tempNilai = 0;

        for(int i = 0; i < komponenPenilaian.length; i++){
            temp += this.komponenPenilaian[i].toString() + "\n";
            tempNilai += this.komponenPenilaian[i].getNilai();
        }
        return String.format("%s\nNilai Akhir : %.2f\nHuruf : %s\n%s", temp, tempNilai, Mahasiswa.getHuruf(tempNilai), Mahasiswa.getLulus(tempNilai));

    }

    public String toString(){
        return this.npm + " - " + this.nama;
    }

    public String getDetail(){
        String detil = "";
        double nilai = 0;
        for(int i = 0; i < this.komponenPenilaian.length; i++){
            nilai += this.komponenPenilaian[i].getNilai();
            detil += this.komponenPenilaian[i].getDetail() + "\n";
        }
        return String.format("%s\nNilai Akhir : %.2f\nHuruf : %s\n%s", detil, nilai, Mahasiswa.getHuruf(nilai), Mahasiswa.getLulus(nilai));
    }

    public String getNPM(){
        return this.npm;
    }

    private static String getLulus(double nilaiAkhir){
        if(nilaiAkhir < 55){
            return("TIDAK LULUS");
        }else{
            return("LULUS");
        }
    }

    private static String getHuruf(double nilai){
        if(nilai >= 85){
            return "A";
        }else if(nilai >= 80){
            return "A-";
        }else if(nilai >= 75){
            return "B+";
        }else if(nilai >= 70){
            return "B";
        }else if(nilai >= 65){
            return "B-";
        }else if(nilai >= 60){
            return "C+";
        }else if(nilai >= 55){
            return "C";
        }else if(nilai >= 40){
            return "D";
        }else{
            return("E");
        }
    }
}