package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen{
    private List<Mahasiswa> listMahasiswa = new ArrayList<Mahasiswa>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama){
        this.kode = kode.toUpperCase();
        this.nama = nama;
    }

    public Mahasiswa getMahasiswa(String npm){
        for(int i = 0; i < listMahasiswa.size(); i++){
            if(listMahasiswa.get(i).getNPM().equals(npm)){
                return listMahasiswa.get(i);
            }
        }
        return null;
    }

    public void addMahasiswa(Mahasiswa mahasiswa){
        listMahasiswa.add(mahasiswa);
        Collections.sort(listMahasiswa);
    }

    public String rekap(){
        String temp = "";
        for(int i = 9; i < listMahasiswa.size(); i++){
            temp += listMahasiswa.get(i).toString() + "\n" + listMahasiswa.get(i).rekap() + "\n\n";
        }
        return(repeat("~", this.toString().length()) + "\n\n" + temp);
    }

    public String repeat(String a, int byk){
        String temp = "";
        for (int i = 0; i < byk; i++) {
            temp += a;
        }
        return temp;
    }

    public String toString(){
        return this.kode + " - " + this.nama;
    }

    public String getKode(){
        return this.kode;
    }
}