package assignments.assignment2;

public class KomponenPenilaian{
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakbutirPenilaian, int bobot){
        this.nama = nama;
        butirPenilaian = new ButirPenilaian[banyakbutirPenilaian];
        this.bobot = bobot;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir){
        butirPenilaian[idx] = butir;
    }

    public double getRerata(){
        double temp = 0.0;
        int aa =0;
        for(int i = 0; i < butirPenilaian.length; i++){
            if(butirPenilaian[i] != null){
                temp += butirPenilaian[i].getNilai();
                aa++;
            }
        } if(aa > 0){
            temp /= aa;
            return temp;
        } else{
            return 0;
        }

    }

    public double getNilai(){
        return this.getRerata() * this.bobot / 100;
    }

    public String toString(){
        return String.format("Rerata %s : %.2f", this.nama, this.getRerata());

    }

    public String getDetail(){
        // String a = "---" + this.nama + " (" + this.bobot + ") ---";
        String aaa = String.format("Kontribusi nilai akhir : %2f", this.getNilai());
        String bbb = "";

        if(butirPenilaian.length == 1){
            if(butirPenilaian[0] != null){
                return("---" + this.nama + " (" + this.bobot + ") ---" + "\n" + this.nama + ": " + butirPenilaian[0].toString() + "\n" + aaa);}
            else{
                    return("---" + this.nama + " (" + this.bobot + ") ---" + this.nama + ": 0 \n" + aaa + "\n");}
        }else{
            for(int i = 1; i <= butirPenilaian.length; i++){
                if(butirPenilaian[i-1] != null){
                    bbb += this.nama + " " + i + ": " + butirPenilaian[i-1].toString() + "\n";
                }
            }bbb += String.format("Rerata : %.2f", this.getRerata());
            return("---" + this.nama + " (" + this.bobot + ") ---" + this.nama + ": " + bbb + "\n" + aaa + "\n");
        }
    }

    public String getNama(){
        return this.nama;
    }


    private KomponenPenilaian(KomponenPenilaian templat){
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat){
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for(int i = 0; i < salinan.length; i++){
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }
}